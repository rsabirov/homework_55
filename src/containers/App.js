import React, { Component } from 'react';
import './App.css';
import Board from '../components/board/Board';
import Counter from "../components/counter/Counter";
import ResetBtn from "../components/reset-button/ResetBtn";
import Cell from "../components/cell/Cell";

class App extends Component {

  state = {
    cells: [],
    random: 0,
    total: 36,
    openedCells: [],
  };

  componentDidMount() {
    this.generateCells();
    this.addRingToRandom();
  };

  generateCells = () => {
    const cells = [];

    for (let i = 0; i < 36; i++) {
      cells.push('cell__' + i);
    }

    this.setState({cells});
  };

  cellClickHandler = (cell) => {
    console.log(this.state.openedCells);
    this.setState({openedCells: [...this.state.openedCells, cell]});
  };

  resetClickHandler = () => {
    this.setState({openedCells: [], random: null});
    this.addRingToRandom();
  };

  addRingToRandom = () => {
      let random = this.state.random;

      random = Math.round(Math.random() * this.state.total);

      this.setState({random: random});
      console.log(this.state.random);
  };

  render() {

    return (
      <div className="App">
        <div className='container'>
          <Board>
            {this.state.cells.map(cell =>
              <Cell key={cell} onClick={this.cellClickHandler} random={this.state.random} cell={cell} isOpen={this.state.openedCells.indexOf(cell) >= 0}/>
            )}
          </Board>
          <Counter tries={this.state.openedCells.length} />
          <ResetBtn clickHandler={this.resetClickHandler} />
        </div>
      </div>
    );
  }
}

export default App;
