import React from 'react';

const Counter = (props) =>{
  return (
    <div>
      <p>Tries: {props.tries}</p>
    </div>
  )
};

export default Counter;