import React from 'react';
import  "./cell.css";

const Cell = (props) =>{
  const className = ['cell'];

  if (props.isOpen) {
    className.push('open');
  }

  if (('cell__' + props.random) === props.cell) {
      className.push('has-ring');
  }

   const generateSpan = () => {
       return <span className="ring"></span>;
   };

    return (
      <div className={className.join(' ')} onClick={() => props.onClick(props.cell)}>
          {
              ('cell__' + props.random) === props.cell ? generateSpan() : null
          }
      </div>
  )
};

export default Cell;