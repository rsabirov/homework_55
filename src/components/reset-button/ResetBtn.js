import React from 'react';
import  "./reset-btn.css";

const ResetBtn = (props) =>  {
  return (
    <div className="reset">
      <button className="reset__btn" onClick={() => props.clickHandler()}>Reset</button>
    </div>
  )
};

export default ResetBtn;